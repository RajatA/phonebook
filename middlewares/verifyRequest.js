const jwtToken = require('../util/JwtToken');

module.exports = async (req, res, next) => {
    try {
        if (!req.headers.authorization) {
            return res.status(401).json({
                status: 401,
                message: 'UNAUTHORIZED_REQUEST',
            });
        }
    
        idToken = req.headers.authorization.split(' ')[1];
    
        const userProfile = await jwtToken.verify(idToken);
    
        req.userProfile = userProfile;
    
        next();
    } catch (err) {
        console.log("Error : ", err);
        return res.status(401).json({message: 'Invalid request'})   
    }
}