# Phonebook

Simple phone book CRUD app.

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install dependencies.

```bash
npm install
```

## Usage

* Import [Postman](https://www.getpostman.com/)-collection files in postman
* Then select Postman-environment from Environment selection.

#### SignUp
##### POST 
```json
http://localhost:5000/auth/v1/signup

JSON Body:
{
    "name": "ABC",
    "email": "abc@g.c",
    "password": "pass",
    "gender": "male"
} 
```
----

#### SignIn
##### POST 

```json
http://localhost:5000/auth/v1/signin

JSON Body:
{
    "email": "abc@g.c",
    "password": "pass"
}
```
----

#### Save Contact on Phonebook
##### POST 

```json
localhost:5000/phonebook/v1

JSON Body:
{
	"name": "Kool",
	"number": 343434,
	"address": "wfwf fwef"
}
```
---
#### Updated Contact on Phonebook
##### PUT

```json
localhost:5000/phonebook/v1

JSON Body:
{
     "_id": "5dd0f8737bd2631e47185283",
     "name": "Kool-boy",
     "number": 99999999,
     "address": "wfwf fwef"
} 
```

---
#### Get Contacts from Phonebook
##### GET

```json
localhost:5000/phonebook/v1?page=2
```

---
#### Delete Contact from Phonebook
##### DELETE

```json
localhost:5000/phonebook/v1

JSON Body:
{
    "_id": "5dd0f8737bd2631e47185283"
}
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)