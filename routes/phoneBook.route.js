const router = require('express').Router();
const phBkModel = require('../models/phoneBook.model');

router.post('/', (req, res) => {
    const { name, number, address } = req.body;
    const phBook = {
        name: name,
        number: number,
        address: address,
        createdBy: req.userProfile.user._id
    };
    const db = new phBkModel(phBook);
    db.save((err, doc) => {
        if (err) {
            console.log('Something went wrong', err);
            res.status(500).json({ message: "Something went wrong!" });
        } else {
            console.log('Data saved successfully!');
            res.json(doc);
        }
    });
});

router.get('/', async (req, res) => {
    try {
        let page;
        const limit = 5;

        if(req.query.page) 
            page = req.query.page;
        else 
            page = 1;

        if(page === '0') {
            page = 1;
        }
        const skips = limit * (page - 1);

        const numberOfDocs = await phBkModel.count({createdBy: req.userProfile.user._id}).exec();
        const pages = Math.ceil(numberOfDocs / limit);
        const docs = await phBkModel.find({createdBy: req.userProfile.user._id}).skip(skips).limit(limit).select("-createdBy -__v").exec();

        return res.json({
            numberofDocPresent: numberOfDocs,
            totalPages: pages,
            docs: docs
        });
    } catch (err) {
        console.log('Something went wrong', err);
        res.status(500).json({ message: "Something went wrong!" });
    }
});

router.put('/', (req, res) => {
    const { _id, name, number, address } = req.body;
    phBkModel.updateOne({ _id: _id }, { name: name, number: number, address: address }, (err, doc) => {
        if (err) {
            console.log('Something went wrong', err);
            res.status(500).json({ message: "Something went wrong!" });
        } else {
            console.log('Data saved successfully!');
            res.json({ message: 'record Updated successfully', data: req.body});
        }
    });
});

router.delete('/', (req, res) => {
    const { _id } = req.body;
    phBkModel.deleteOne({ _id: _id }, (err) => {
        if (err) {
            console.log('Something went wrong');
            res.status(500).json({ message: "Something went wrong!" });
        } else {
            console.log('Data saved successfully!');
            res.json({ message: 'record Deleted' });
        }
    });
});

module.exports = router;