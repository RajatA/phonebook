const router = require('express').Router();
const uProfileModel = require('../models/userProfile.model');
const jwtToken = require('../util/JwtToken');

router.post('/signup', async (req, res) => {
    const { name, email, password, gender } = req.body;

    const result = await uProfileModel.findOne({ email: email }).exec();

    if(result) {
        return res.status(401).json({message: "User already registered with this emaiId"});
    }

    const user = {
        name: name,
        email: email,
        password: password,
        gender, gender
    }

    const model = new uProfileModel(user);

    model.save((err, doc) => {
        if (err) {
            console.log('Something went wrong');
            res.status(500).json({ message: "Something went wrong!" });
        } else {
            console.log('Data saved successfully!');
            res.json({ message: 'record User Profile saved' });
        }
    });
});

router.post('/signin', async (req, res) => {
    try {
        const { email, password } = req.body;

        const result = await uProfileModel.findOne({ email: email, password: password }).exec();

        if (result) {
            // Generate token.
            const token = await jwtToken.generate(result);
            return res.json({ user: result, token: token });
        } else {
            // User not found.
            return res.status(404).json({ message: 'USER_NOT_FOUND'});
        }
    } catch (err) {
        console.log('Error : ', err);
        return res.json({ message: 'Something went wrong' });
    }
});

module.exports = router;