const fs = require('fs');
const jwt = require('jsonwebtoken');

const privateKey = fs.readFileSync('./config/private.key', 'utf8');
const publicKey = fs.readFileSync('./config/public.key', 'utf8');

const signOptions = {
    issuer: 'Phonebok',
    subject: 'users@phonebook.com',
    audience: 'phonebook.com',
    expiresIn: '24h',
    algorithm: 'RS256'
};

module.exports = {
    generate: (payload) => {
        return new Promise((resolve, reject) => {
            try {
                jwt.sign({user:payload}, privateKey, signOptions, (err, token) => {
                    console.log(token);
                    if (err) {
                        console.log("Error : ", err);
                        return reject(err);
                    } else {
                        console.log("Token is : ", token);
                        return resolve(token);
                    }
                });
            } catch (err) {
                return reject(err);
            }
        });
    },
    verify: (token) => {
        return new Promise((resolve, reject) => {
            try {
                jwt.verify(token, publicKey, signOptions, function(err, decodedToken) {
                    if(err) {
                        return reject({
                            status: 401,
                            code: 'API_AUTH_INVALID_TOKEN',
                            message: 'API_AUTH_INVALID_TOKEN',
                            error: err.message,
                            stack : err.message
                        });
                    } else {
                        return resolve(decodedToken);
                    }
                });
            } catch (err) {
                return reject(err);
            }
        });
    }
}