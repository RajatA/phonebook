// The objective of this task is to make the below code more efficient. 
// Comments mentioned below are to help you understand the code, but in case you have more questions, please shoot us an email. 
// You have all the liberty to modify/change with the code, its structure and so on. 
// No external libraries to be included.

user.newRegister = async function () {
    try {
        // To check if User exists in Users DB
        const user = await userDB.findOne({}).exec();
        let result;
        if (user) {
            // To check if User exists in Directory DB
            return Directory.getUser(user, user.id).then(() => {
                result = { user, is_registered: true };
            }).catch(err => {
                if (err.message.indexOf('does not exist') > -1) {
                    result = { user, is_registered: false };
                }
            });
        } else {
            // Enrolling the User in Enrolment DB
            const creds = await Enrolment.enrollUser(AdminUsername, AdminPassword);
            keypair = GPG.generateKeypair();
            const myguy = new this({
                name: 'Some Name',
                identity: {
                    type: creds['type'],
                    certificate: creds['certificate'],
                    private_key: creds['privateKey'],
                    public_key: creds['publicKey']
                },
                keypair: {
                    private_key: keypair.privateKey,
                    public_key: keypair.publicKey
                }
            });
            // Saving the User in Users DB
            const savedUser = await myguy.save();
            result = { user: savedUser, is_registered: false }
        }
        return await saveUser(result);
    } catch (err) { // handle error
        return { message: 'something went wrong' };
    }
}

// Saving the User in Directory DB
const saveUser = (({ user, is_registered }) => {
    return new Promise(async (resolve, reject) => {
        if (is_registered === false) {
            // Saving the User in Directory DB
            await Directory.createUser(user);
            return resolve(
                User.findOneAndUpdate({ _id: user._id }, {
                    _id: user._id,
                    name: user.name,
                }, {
                    upsert: true,
                    setDefaultsOnInsert: true,
                    useFindAndModify: false
                }).exec()
            );
        } else {
            return resolve(user);
        }
    });
});