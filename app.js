const app = require('express')();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const authenticateRequest = require('./middlewares/verifyRequest');

mongoose.connect('mongodb://testuser:admin123@ds051841.mlab.com:51841/phone-book', (err) => {
    if (err)
        console.log('Error in mongoose..', err);
    else
        console.log('Mongoose connected...');
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

const PORT = 5000;

app.use('/auth/v1', require('./routes/auth.route'));

app.use(authenticateRequest);

app.use('/phonebook/v1', require('./routes/phoneBook.route'));

app.listen(PORT, (args) => console.log('Server listening on port ', PORT));