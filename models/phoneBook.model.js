const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const phoneBook = new Schema({
    name: String,
    number : Number,
    address : String,
    createdBy: Schema.Types.ObjectId
});

module.exports = mongoose.model('phoneBook', phoneBook);