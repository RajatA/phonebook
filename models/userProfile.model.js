const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userProfile = new Schema({
    name: String,
    email: String,
    gender: String,
    password: String,
});

module.exports = mongoose.model('userProfile', userProfile);